use std::{
	fmt::{Display, Write},
	str::FromStr,
	time::UNIX_EPOCH,
};

use mime_guess::Mime;
use serde::Serialize;
use serde_repr::Serialize_repr;

#[derive(Serialize_repr, PartialEq, Eq, Clone, Copy, Debug)]
#[repr(u8)]
pub enum VfsEntryType {
	File = 1,
	Directory = 2,
	Symlink = 3,
}

#[derive(Serialize, Debug)]
pub struct VfsSearchMime {
	pub r#type: String,
	pub subtype: String,
}

impl Display for VfsSearchMime {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(&self.r#type)?;
		f.write_char('/')?;
		f.write_str(&self.subtype)
	}
}

impl TryInto<Mime> for VfsSearchMime {
	type Error = mime_guess::mime::FromStrError;

	fn try_into(self) -> Result<Mime, Self::Error> {
		Mime::from_str(&self.to_string())
	}
}

impl From<Mime> for VfsSearchMime {
	fn from(value: Mime) -> Self {
		Self {
			r#type: value.type_().to_string(),
			subtype: value.subtype().to_string(),
		}
	}
}

#[derive(Serialize, Debug)]
pub struct VfsSearchEntry {
	pub id: u64,
	pub name: String,
	pub path: String,
	pub path_ids: Vec<u64>,
	pub r#type: VfsEntryType,
	pub meta: VfsMetadata,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub mime: Option<VfsSearchMime>,
}

#[derive(Serialize, Debug)]
pub struct VfsMetadata {
	pub len: u64,
	#[serde(default, skip_serializing_if = "Option::is_none")]
	pub created: Option<i64>,
	#[serde(default, skip_serializing_if = "Option::is_none")]
	pub modified: Option<i64>,
}

impl TryFrom<std::fs::Metadata> for VfsMetadata {
	type Error = std::io::Error;

	fn try_from(value: std::fs::Metadata) -> Result<Self, Self::Error> {
		Ok(Self {
			len: value.len(),
			created: value
				.created()
				.ok()
				.and_then(|t| t.duration_since(UNIX_EPOCH).ok())
				.and_then(|d| d.as_millis().try_into().ok()),
			modified: value
				.modified()
				.ok()
				.and_then(|t| t.duration_since(UNIX_EPOCH).ok())
				.and_then(|d| d.as_millis().try_into().ok()),
		})
	}
}
