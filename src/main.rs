use std::{
	collections::HashSet,
	path::PathBuf,
	sync::{
		atomic::{AtomicU64, Ordering},
		Arc,
	},
	thread,
	time::{Duration, Instant},
};

use clap::Parser;
use indicatif::{ProgressBar, ProgressStyle};
use meilisearch_sdk::client::Client;
use model::{VfsMetadata, VfsSearchEntry};
use tokio::sync::mpsc::channel;
use walker::Walker;

mod model;
mod walker;

#[derive(Parser, Debug)]
pub struct Opts {
	/// The base directory or directories to index.
	pub dirs: Vec<PathBuf>,

	/// Optional excluded paths, relative to the base directories and without leading and trailing slashes.
	#[clap(short, long)]
	pub exclude: Vec<PathBuf>,

	/// The batch size for Meilisearch uploads
	#[clap(short, long, default_value_t = 64 * 1024)]
	pub batch_size: usize,

	/// Meilisearch server URL
	#[clap(short, long, default_value = "http://localhost:7700")]
	pub url: String,

	/// Meilisearch API key
	#[clap(short, long)]
	pub key: Option<String>,

	/// Meilisearch index name
	#[clap(short, long)]
	pub index: String,
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
	tracing_subscriber::fmt::init();

	let opts = Opts::parse();
	tracing::info!(?opts);

	let pb = ProgressBar::new_spinner();
	pb.enable_steady_tick(Duration::from_millis(80));
	pb.set_style(ProgressStyle::with_template("{spinner} {msg}").unwrap().tick_strings(&[
		"⢀⠀", "⡀⠀", "⠄⠀", "⢂⠀", "⡂⠀", "⠅⠀", "⢃⠀", "⡃⠀", "⠍⠀", "⢋⠀", "⡋⠀", "⠍⠁", "⢋⠁", "⡋⠁", "⠍⠉", "⠋⠉", "⠋⠉", "⠉⠙", "⠉⠙", "⠉⠩",
		"⠈⢙", "⠈⡙", "⢈⠩", "⡀⢙", "⠄⡙", "⢂⠩", "⡂⢘", "⠅⡘", "⢃⠨", "⡃⢐", "⠍⡐", "⢋⠠", "⡋⢀", "⠍⡁", "⢋⠁", "⡋⠁", "⠍⠉", "⠋⠉", "⠋⠉", "⠉⠙",
		"⠉⠙", "⠉⠩", "⠈⢙", "⠈⡙", "⠈⠩", "⠀⢙", "⠀⡙", "⠀⠩", "⠀⢘", "⠀⡘", "⠀⠨", "⠀⢐", "⠀⡐", "⠀⠠", "⠀⢀", "⠀⡀",
	]));
	pb.set_message("Initializing...");

	let client = Client::new(opts.url, opts.key).unwrap();
	let mut index = client.index(opts.index);
	index.delete_all_documents().await.unwrap();
	index.set_searchable_attributes(&["name"]).await.unwrap();
	index.set_sortable_attributes(&["name"]).await.unwrap();
	index
		.set_filterable_attributes(&["id", "path_ids", "path", "mime.type"])
		.await
		.unwrap();
	index.set_primary_key("id").await.unwrap();

	let (tx, mut rx) = channel(4096);
	let id = Arc::new(AtomicU64::new(0));
	let excludes = HashSet::from_iter(opts.exclude);
	let total_size = Arc::new(AtomicU64::new(0));

	// Start walkers
	let threads = opts
		.dirs
		.into_iter()
		.map(|dir| {
			let tx = tx.clone();
			let id = id.clone();
			let excludes = excludes.clone();
			let total_size = total_size.clone();
			thread::spawn(move || {
				let mut walker = Walker::new(dir.clone(), id, tx, excludes);
				let size = walker.walk(&dir);
				tracing::debug!(?dir, "done");
				total_size.fetch_add(size, Ordering::Relaxed);
			})
		})
		.collect::<Vec<_>>();

	drop(tx);

	// Submit batches
	let start = Instant::now();
	let mut next_update = Instant::now();
	let mut entry_count = 0;
	let mut batch = Vec::with_capacity(opts.batch_size);
	while let Some(entry) = rx.recv().await {
		entry_count += 1;
		if Instant::now() > next_update {
			next_update = Instant::now() + Duration::from_millis(80);
			pb.set_message(format!("Indexed {} items - {}", entry_count, entry.path));
		}

		batch.push(entry);
		if batch.len() >= opts.batch_size {
			tracing::debug!(len = batch.len(), "batch");
			index.add_documents(&batch, None).await.unwrap();
			batch.clear();
		}
	}

	// Wait for walker threads
	for h in threads {
		h.join().unwrap();
	}

	// Root directory
	batch.push(VfsSearchEntry {
		id: 0,
		name: String::new(),
		path: String::new(),
		path_ids: Vec::new(),
		r#type: model::VfsEntryType::Directory,
		meta: VfsMetadata {
			len: total_size.load(Ordering::Relaxed),
			created: None,
			modified: None,
		},
		mime: None,
	});

	// Submit last batch
	tracing::debug!(len = batch.len(), "batch");
	index.add_documents(&batch, None).await.unwrap();

	let end = Instant::now();

	pb.finish_with_message(format!(
		"Indexing complete after {:.1} seconds, found {} items",
		(end - start).as_secs_f64(),
		entry_count
	));
}
