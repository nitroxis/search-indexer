use std::{
	collections::HashSet,
	fs::{read_dir, DirEntry},
	path::{Path, PathBuf},
	sync::{
		atomic::{AtomicU64, Ordering},
		Arc,
	},
};

use tokio::sync::mpsc::Sender;

use crate::model::{VfsEntryType, VfsMetadata, VfsSearchEntry};

pub struct Walker {
	base_path: PathBuf,
	id: Arc<AtomicU64>,
	tx: Sender<VfsSearchEntry>,
	path_ids: Vec<u64>,
	excludes: HashSet<PathBuf>,
}

impl Walker {
	pub fn new(base_path: PathBuf, id: Arc<AtomicU64>, tx: Sender<VfsSearchEntry>, excludes: HashSet<PathBuf>) -> Self {
		Self {
			base_path,
			id,
			tx,
			path_ids: vec![0],
			excludes,
		}
	}

	fn visit(&mut self, entry: DirEntry) -> u64 {
		let path = entry.path();

		let rel_path = match path.strip_prefix(&self.base_path) {
			Ok(p) => p,
			Err(e) => {
				tracing::warn!(%e, ?path, "strip prefix");
				return 0;
			}
		};

		if self.excludes.contains(rel_path) {
			tracing::debug!(?rel_path, "excluded");
			return 0;
		}

		let rel_path = match rel_path.to_str() {
			Some(path) => path.to_owned(),
			None => {
				tracing::warn!(?rel_path, "invalid path");
				return 0;
			}
		};

		let name = match entry.file_name().to_str() {
			Some(name) => name.to_owned(),
			None => {
				tracing::warn!(name = ?entry.file_name(), "invalid file name");
				return 0;
			}
		};

		let meta = match entry.metadata() {
			Ok(meta) => meta,
			Err(e) => {
				tracing::warn!(%e, ?path, "metadata");
				return 0;
			}
		};

		let mut meta: VfsMetadata = match meta.try_into() {
			Ok(meta) => meta,
			Err(e) => {
				tracing::warn!(%e, ?path, "convert metadata");
				return 0;
			}
		};

		let ty = match entry.file_type() {
			Ok(ty) => {
				if ty.is_dir() {
					VfsEntryType::Directory
				} else if ty.is_symlink() {
					VfsEntryType::Symlink
				} else {
					VfsEntryType::File
				}
			}
			Err(e) => {
				tracing::warn!(%e, ?path, "file_type");
				return 0;
			}
		};

		let id = self.id.fetch_add(1, Ordering::Relaxed);

		let mime = mime_guess::from_path(&path).first().map(|m| m.into());

		if matches!(ty, VfsEntryType::Directory) {
			self.path_ids.push(id);
			let len = self.walk(path);
			meta.len = len;
			self.path_ids.pop();
		}

		let len = meta.len;

		self.tx
			.blocking_send(VfsSearchEntry {
				id,
				path: rel_path,
				path_ids: self.path_ids.clone(),
				name,
				r#type: ty,
				meta,
				mime,
			})
			.unwrap();

		len
	}

	pub fn walk(&mut self, path: impl AsRef<Path>) -> u64 {
		let path = path.as_ref();
		let mut len_sum = 0;
		match read_dir(path) {
			Ok(dir) => {
				for entry in dir {
					match entry {
						Ok(entry) => {
							len_sum += self.visit(entry);
						}
						Err(e) => {
							tracing::warn!(%e, ?path, "read_dir entry");
						}
					}
				}
			}
			Err(e) => {
				tracing::warn!(%e, ?path, "read_dir");
			}
		}

		len_sum
	}
}
